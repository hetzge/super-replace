package de.hetzge.superreplace.transform;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.util.CheckClassAdapter;
import org.pmw.tinylog.Logger;

import de.hetzge.superreplace.Replacement;
import de.hetzge.superreplace.SuperReplaceConstants;
import de.hetzge.superreplace.Utils;

/**
 * Transforms the classes by the given {@link Replacement} rules.
 */
public class ReplaceInstantiationClassFileTransformer implements ClassFileTransformer {

	/**
	 * {@link Replacement} rules which should be applied by the transformation.
	 */
	private final Set<Replacement> replacements;

	/**
	 * A {@link Predicate} which decides if the transformation should be applied to
	 * a specific class.
	 */
	private final Predicate<String> classFilterPredicate;

	public ReplaceInstantiationClassFileTransformer(Collection<Replacement> replacements,
			Predicate<String> classFilterPredicate) {
		if (replacements == null || replacements.isEmpty()) {
			throw new IllegalArgumentException("'replacements' are null or empty");
		}
		this.replacements = new HashSet<>(replacements);
		this.classFilterPredicate = classFilterPredicate;
	}

	@Override
	public byte[] transform(ClassLoader loader, String internalClassName, Class<?> classBeingRedefined,
			ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
		{
			try {
				if (internalClassName != null && classFilterPredicate.test(Utils.internalToClassName(internalClassName))
						&& !Utils.isProxy(internalClassName)) {
					final ClassReader reader = new ClassReader(classfileBuffer);
					final ClassWriter writer = new ClassWriter(0);

					ClassVisitor visitor = new CheckClassAdapter(writer, true);
					for (Replacement replacement : replacements) {
						visitor = new ReplaceInstantiationClassVisitor(SuperReplaceConstants.API, visitor, replacement,
								internalClassName);
					}

					// begin visiting the bytecode (start the transformation)
					reader.accept(visitor, 0);

					Logger.debug(String.format("applied replace transformation to '%s'", internalClassName));
					return writer.toByteArray();
				} else {
					// return null to signal no change
					return null;
				}
			} catch (Exception e) {
				Logger.error(String.format(
						"Error while transform class bytecode. (internalClassName: %s, classBeingRedefined: %s, bytes: %s)",
						internalClassName, classBeingRedefined, classfileBuffer.length));
				Logger.error(e);
				throw new IllegalStateException();
			}
		}
	}
}