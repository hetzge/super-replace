package de.hetzge.superreplace;

import java.util.Collection;

import org.pmw.tinylog.Logger;

import de.hetzge.superreplace.hotswapagent.HotswapAgentPlugin;
import de.hetzge.superreplace.instrumentation.Agent;

/**
 * This class is the main accessing point for clients to use the super-replace
 * functionality.
 */
public final class SuperReplace {

	private SuperReplace() {
	}

	public static void retransform(Collection<Replacement> replacements, ClassFilterPredicate classFilterPredicate) {
		Agent.retransform(replacements, classFilterPredicate);
	}

	// TODO load property file
	public static void retransform() {
		Agent.retransform(Scanner.scanReplaceAnnotations(), c -> true);
	}

	public static void setupHotswapAgentPlugin(Collection<Replacement> replacements,
			ClassFilterPredicate classFilterPredicate) {
		try {
			HotswapAgentPlugin.register(replacements, classFilterPredicate);
		} catch (NoClassDefFoundError e) {
			Logger.warn("Failed to setup super-replace HotswapAgent plugin. Eventually HotswapAgent is not used.");
		}
	}
}
