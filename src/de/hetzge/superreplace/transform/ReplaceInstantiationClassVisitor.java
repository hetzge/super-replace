package de.hetzge.superreplace.transform;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;

import de.hetzge.superreplace.Replacement;
import de.hetzge.superreplace.Utils;

/**
 * Visitor responsible for visiting the content of a class.
 */
public class ReplaceInstantiationClassVisitor extends ClassVisitor {

	/**
	 * The replacement the visitor is responsible for.
	 */
	private final Replacement replacement;

	/**
	 * The class name of the class the visitor is visiting.
	 */
	private final String className;

	public ReplaceInstantiationClassVisitor(int api, ClassVisitor cv, Replacement replacement, String className) {
		super(api, cv);
		this.replacement = replacement;
		this.className = className;
	}

	@Override
	public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
		final MethodVisitor superMethodVisitor = super.visitMethod(access, name, desc, signature, exceptions);

		if (!Utils.isBaseClass(replacement.getReplacementClass(), className)
				&& !className.equals(Type.getInternalName(replacement.getReplacementClass()))) {
			return new ReplaceInstantiationMethodVisitor(api, superMethodVisitor, replacement, name);
		} else {
			return superMethodVisitor;
		}
	}
}
