package de.hetzge.superreplace;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.objectweb.asm.Type;
import org.pmw.tinylog.Level;
import org.pmw.tinylog.Logger;

/**
 * Collection of general functions.
 */
public final class Utils {
	private Utils() {
	}

	/**
	 * Parses a comma separated list from a {@link String}. A example {@link String}
	 * might look like <code>"one,two,three"</code>. Spaces around the single values
	 * will be trimmed. Empty values will not be contained in the result.
	 * 
	 * @param value The value to parse.
	 * @return A list with the single values contained in the provided comma
	 *         separated {@link String}.
	 */
	public static List<String> parseCommaSeperatedValue(String value) {
		if (value != null) {
			return Stream.of(value.split(",")).map(String::trim).filter(s -> !s.isEmpty()).collect(Collectors.toList());
		} else {
			return Collections.emptyList();
		}
	}

	/**
	 * Parses a given class name to a {@link Class} object. Throws a unchecked
	 * exception instead of {@link ClassNotFoundException}. Therefore it can be used
	 * in java stream processing.
	 * 
	 * @param className The name of the class to parse.
	 * @return {@link Class}
	 */
	public static Class<?> parseClass(String className) {
		try {
			return Class.forName(className);
		} catch (ClassNotFoundException e) {
			throw new IllegalArgumentException(String.format(
					"Class for name %s not found. Please ensure that you added the required dependencies to the classpath.",
					className));
		}
	}

	/**
	 * Checks if the given {@link Class} is a subclass of a class name (like from
	 * {@link Class#getName()}) represented other {@link Class}.
	 * 
	 * @param clazz         The {@link Class} where to check if
	 *                      <code>baseClassName</code> is a superclass from.
	 * @param baseClassName The name of the base class.
	 * @return <code>true</code> if <code>baseClassName</code> is a superclass of
	 *         <code>clazz</code>
	 */
	public static boolean isBaseClass(Class<?> clazz, String baseClassName) {
		Class<?> superClass = clazz;
		while ((superClass = superClass.getSuperclass()) != null) {
			if (superClass.getName().equals(baseClassName)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Checks if the given <code>className</code> is a inner class of the
	 * {@link Class} represented by the given <code>ofClassName</code>.
	 * 
	 * @param clazz       The class name of the class to check if it is inner class
	 *                    of other.
	 * @param ofClassName The class name of the outer {@link Class}
	 * @return <code>true</code> if
	 *         <code> is a inner class of <code>ofClassName</code>
	 */
	public static boolean isInnerClassOf(String className, String ofClassName) {
		return className.startsWith(ofClassName + "$");
	}

	/**
	 * Converts a class name from internal notation to normal notation.
	 * 
	 * @param internalClassName class name in internal format (with '/' instead '.')
	 * @return class name in format like from {@link Class#getName()}
	 */
	public static String internalToClassName(String internalClassName) {
		if (internalClassName != null) {
			return internalClassName.replaceAll("/", ".");
		} else {
			return null;
		}
	}

	/**
	 * Log the given {@link Replacement}s.
	 * 
	 * @param replacements the {@link Replacement}s to log
	 */
	public static void logReplacementConfigs(Collection<Replacement> replacements) {
		if (Logger.getLevel().ordinal() <= Level.INFO.ordinal()) {
			final int maxClassToReplaceLength = replacements.stream().map(Replacement::getClassToReplace)
					.map(Type::getInternalName).map(String::length).max(Integer::compareTo).orElse(0);
			for (Replacement replacement : replacements) {
				Logger.info(String.format(
						"replace %-" + maxClassToReplaceLength + "s with %s (replace class usage: %s)",
						Type.getInternalName(replacement.getClassToReplace()),
						Type.getInternalName(replacement.getReplacementClass()), replacement.isReplaceClassUsage()));
			}
		}
	}

	/**
	 * It is highly recommended that a replacing class provide the same amount
	 * constructors with the same signatures like the class to replace. This method
	 * logs violations to this rule.
	 * 
	 * @param replacements The {@link Replacement}s to check against the constructor
	 *                     rule.
	 */
	public static void logMissingConstructors(Collection<Replacement> replacements) {
		for (Replacement replacement : replacements) {
			final Class<?> replacementClass = replacement.getReplacementClass();
			final Class<?> classToReplace = replacement.getClassToReplace();
			final List<Constructor<?>> missingConstructors = replacement.getMissingConstructors();

			if (!missingConstructors.isEmpty()) {
				Logger.warn("!!! Class '{}' is not fullfill the required constructors of the class to replace '{}' !!!",
						replacementClass.getName(), classToReplace.getName());
				for (Constructor<?> missingConstructor : missingConstructors) {
					Logger.warn("Missing: {}", missingConstructor.toGenericString());
				}
			}
		}
	}

	/**
	 * Logs all conflicting replacements in the given <code>replacements</code>
	 * collection.
	 * 
	 * @param replacements the {@link Replacement}s to check
	 * @see #getConflictingReplacements(Collection)
	 */
	public static void logConflictingReplacements(Collection<Replacement> replacements) {
		final List<Replacement[]> conflicts = getConflictingReplacements(replacements);
		for (Replacement[] conflict : conflicts) {
			Logger.warn("Conflict: The classes '{}' and '{}' try to replace the same class '{}'.",
					conflict[0].getReplacementClass().getName(), conflict[1].getReplacementClass().getName(),
					conflict[0].getClassToReplace().getName());
		}
	}

	/**
	 * Finds all conflicting {@link Replacement}s. A {@link Replacement} is
	 * conflicting if it tries to replace a class which will be also replaced by a
	 * other class in given <code>replacements</code> collection.
	 * 
	 * @return a list with arrays which contains always two conflicting
	 *         {@link Replacement}s. The array has therefore always a length of 2.
	 */
	public static List<Replacement[]> getConflictingReplacements(Collection<Replacement> replacements) {
		final List<Replacement[]> result = new ArrayList<>();
		for (Replacement a : replacements) {
			for (Replacement b : replacements) {
				if (a != b && a.getClassToReplace().equals(b.getClassToReplace())) {
					result.add(new Replacement[] { a, b });
				}
			}
		}
		return result;
	}

	// TODO
	public static boolean isProxy(String internalClassName) {
		return internalClassName != null
				&& (internalClassName.contains("$Proxy$") || internalClassName.contains("$$$"));
	}

	/**
	 * <p>
	 * Sets the package of super-replace as system package. This is necessary for
	 * jboss/wildfly servers to make the library available across module borders.
	 * </p>
	 * <p>
	 * Inspired by
	 * https://github.com/HotswapProjects/HotswapAgent/blob/28a0fbea7102c079c6618c85466a5a06f4e5c41d/hotswap-agent-core/src/main/java/org/hotswap/agent/HotswapAgent.java
	 * </p>
	 */
	public static void fixJBossModules() {
		final String PROPERTY_KEY = "jboss.modules.system.pkgs";
		final String PACKAGE = SuperReplace.class.getPackage().getName();

		final String oldValue = System.getProperty(PROPERTY_KEY, null);
		System.setProperty(PROPERTY_KEY,
				oldValue == null ? PACKAGE : oldValue + "," + PACKAGE);
	}
}
