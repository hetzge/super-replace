package de.hetzge.superreplace;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Describes a class which should be replaced by another class.
 */
public class Replacement implements Serializable {

	/**
	 * The class which should be replaced by another class.
	 */
	private final Class<?> classToReplace;

	/**
	 * The class which should replace the other class.
	 */
	private final Class<?> replacementClass;

	/**
	 * <code>true</code> if the usage of the class like <code>MyClass.class</code>
	 * should be also replaced with the replacement class.
	 */
	private final boolean replaceClassUsage;

	/**
	 * @see #Replacement(Class, boolean)
	 */
	public Replacement(Class<?> replacementClass) {
		this(replacementClass, isReplaceClassUsage(replacementClass));
	}

	/**
	 * Constructor to create a {@link Replacement} by a {@link Class} which should
	 * replace it superclass.
	 * 
	 * @param replacementClass
	 *            the {@link Class} which should replace it superclass
	 * @param replaceClassUsage
	 *            flag if the usage of the class also should be replaced. See
	 *            {@link #isReplaceClassUsage()}.
	 */
	public Replacement(Class<?> replacementClass, boolean replaceClassUsage) {
		this.classToReplace = replacementClass.getSuperclass();
		this.replacementClass = replacementClass;
		this.replaceClassUsage = replaceClassUsage;
	}

	/**
	 * The replacement class needs to have the same constructors like the replaced
	 * class. This methods checks for inconsistencies.
	 * 
	 * @return constructors from <code>classToReplace</code> which are missing in
	 *         <code>replacementClass</code>.
	 */
	public List<Constructor<?>> getMissingConstructors() {
		final List<Constructor<?>> result = new ArrayList<>();

		final Constructor<?>[] classToReplaceConstructors = classToReplace.getConstructors();
		final Constructor<?>[] replacementClassConstructors = replacementClass.getConstructors();

		toReplace: for (int i = 0; i < classToReplaceConstructors.length; i++) {
			final Constructor<?> classToReplaceConstructor = classToReplaceConstructors[i];
			final Class<?>[] classToReplaceConstructorParameterTypes = classToReplaceConstructor.getParameterTypes();

			for (int j = 0; j < replacementClassConstructors.length; j++) {
				final Constructor<?> replacementClassConstructor = replacementClassConstructors[j];
				final Class<?>[] replacementClassConstructorParameterTypes = replacementClassConstructor
						.getParameterTypes();

				if (Arrays.equals(classToReplaceConstructorParameterTypes, replacementClassConstructorParameterTypes)) {
					continue toReplace;
				}
			}

			result.add(classToReplaceConstructor);
		}

		return result;
	}

	/**
	 * @return the {@link Class} to replace
	 */
	public Class<?> getClassToReplace() {
		return classToReplace;
	}

	/**
	 * @return the replacement {@link Class}
	 */
	public Class<?> getReplacementClass() {
		return replacementClass;
	}

	/**
	 * Getter for {@link #replaceClassUsage}
	 * 
	 * @return if class usage should also be replaced
	 * @see #replaceClassUsage
	 */
	public boolean isReplaceClassUsage() {
		return replaceClassUsage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((classToReplace == null) ? 0 : classToReplace.hashCode());
		result = prime * result + ((replacementClass == null) ? 0 : replacementClass.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Replacement other = (Replacement) obj;
		if (classToReplace == null) {
			if (other.classToReplace != null)
				return false;
		} else if (!classToReplace.equals(other.classToReplace))
			return false;
		if (replacementClass == null) {
			if (other.replacementClass != null)
				return false;
		} else if (!replacementClass.equals(other.replacementClass))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ClassReplacementConfig [classToReplace=" + classToReplace + ", replacementClass=" + replacementClass
				+ ", replaceClassUsage=" + replaceClassUsage + "]";
	}

	/**
	 * Evaluates if the given {@link Class} has a {@link Replace} annotation and
	 * returns the {@link Replace#replaceClassUsage()} property.
	 * 
	 * @param clazz
	 *            The class to evaluate.
	 */
	private static boolean isReplaceClassUsage(Class<?> clazz) {

		final Replace replaceAnnotation = clazz.getAnnotation(Replace.class);
		return replaceAnnotation != null && replaceAnnotation.replaceClassUsage();
	}
}
