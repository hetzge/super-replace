package de.hetzge.superreplace.demo;

import java.io.Serializable;

public class Base implements Serializable {
	private String value;

	public Base() {
		this("BASE");
	}
	
	public Base(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
