**super-replace** is a library to replace instantiation at bytecode level.

---

Imaging you have class you don't have access to its source code and the need to customize it. This library allows to do a transformation like this:

Before:
```java
class SomeComponent {
    public SomeComponent() {
        add( new SubComponent());
    }
}
```

After: 
```java
class SomeComponent {
    public SomeComponent() {
        add( new CustomizedSubComponent());
    }
}
```

To archive this goal super-replace provides the annotation `@Replace`. In the example case the usage could look like: 
```java
@Replace
class CustomizedSubComponent extends SubComponent {
    ...
}
```

---

To build the library run `mvn verify`.
