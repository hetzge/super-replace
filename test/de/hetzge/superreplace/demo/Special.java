package de.hetzge.superreplace.demo;

import de.hetzge.superreplace.Replace;

@Replace
public class Special extends Base {

	public Special() {
		super("SPECIAL");
	}
	
	public Special(String value) {
		super(value);
	}

	@Override
	public String getValue() {
		return "special " + super.getValue();
	}

	public String getBaseValue() {
		return new Base("xyz").getValue();
	}

	public class SpecialInner {
		public String getBaseValue() {
			return new Base("xyz").getValue();
		}
	}
}
