package de.hetzge.superreplace.demo;

import java.io.IOException;

import de.hetzge.superreplace.SuperReplace;

// http://www.baeldung.com/java-asm
// http://asm.ow2.org/license.html
// TODO global transformer
// TODO replace Class.class usages
// TODO replace reflection instantiation
// TODO be carful with pages (MyPage.class)
// TODO @Replace @Replace
// TODO new SuperClass() in inner class
// TODO time measurement
// TODO logging
// TODO behavior with hotswap / jrebel
// TODO transform command line application

/**
 * Run with arguments:
 * <code>-javaagent:"target/super-replace-0.0.1-SNAPSHOT.jar"=scanPackages:de.hetzge.superreplace.demo;includePackages:de.hetzge.superreplace.demo -Dorg.slf4j.simpleLogger.defaultLogLevel=debug</code>
 */
public class Demo {
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		run();
		SuperReplace.retransform();
		run();
	}

	private static void run() throws IOException, ClassNotFoundException {
		final Base base = new Base("hello world");
		final Special special = new Special("hello world");

		execute(base);
		execute(special);
	}

	private static void execute(Base base) {
		System.out.println(base.getValue());
	}
}
