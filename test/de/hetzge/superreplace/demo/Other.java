package de.hetzge.superreplace.demo;

import java.io.Serializable;

public class Other implements Serializable {

	private final Serializable some;

	public Other(Serializable some) {
		this.some = some;
	}

	public Serializable getSome() {
		return some;
	}

	@Override
	public String toString() {
		return String.format("Other [some=%s]", some);
	}
}
