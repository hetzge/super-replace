package de.hetzge.superreplace.hotswapagent;

import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;
import java.util.Collection;

import org.hotswap.agent.annotation.Init;
import org.hotswap.agent.annotation.LoadEvent;
import org.hotswap.agent.annotation.OnClassLoadEvent;
import org.hotswap.agent.annotation.Plugin;
import org.hotswap.agent.command.Scheduler;
import org.hotswap.agent.config.PluginManager;
import org.hotswap.agent.javassist.NotFoundException;
import org.pmw.tinylog.Logger;

import de.hetzge.superreplace.ClassFilterPredicate;
import de.hetzge.superreplace.Replacement;
import de.hetzge.superreplace.transform.ReplaceInstantiationClassFileTransformer;

/**
 * Plugin for HotswapAgent (http://hotswapagent.org)
 */
@Plugin(name = "super-replace", testedVersions = { "" })
public class HotswapAgentPlugin {

	@Init
	private Scheduler scheduler;

	private ReplaceInstantiationClassFileTransformer transformer;

	private void setup(Collection<Replacement> replacements, ClassFilterPredicate classFilterPredicate) {
		transformer = new ReplaceInstantiationClassFileTransformer(replacements, classFilterPredicate);
		Logger.info("setup super-replace HotswapAgent plugin");
	}

	@OnClassLoadEvent(classNameRegexp = ".*", events = { LoadEvent.DEFINE, LoadEvent.REDEFINE })
	public byte[] reloadClass(ClassLoader loader, String internalClassName, Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] classFileBuffer) throws NotFoundException, IllegalClassFormatException {
		Logger.debug("OnClassLoadEvent: " + internalClassName);
		return transformer.transform(loader, internalClassName, classBeingRedefined, protectionDomain, classFileBuffer);
	}

	public static void register(Collection<Replacement> replacements, ClassFilterPredicate classFilterPredicate) {
		final Class<HotswapAgentPlugin> clazz = HotswapAgentPlugin.class;
		
		PluginManager.getInstance().getPluginRegistry().scanPlugins(clazz.getClassLoader(),
				clazz.getPackage().getName());
		PluginManager.getInstance().getPluginRegistry().initializePlugin(clazz.getName(), clazz.getClassLoader());

		final HotswapAgentPlugin plugin = PluginManager.getInstance().getPlugin(clazz, clazz.getClassLoader());
		
		plugin.setup(replacements, classFilterPredicate);
	}
}
