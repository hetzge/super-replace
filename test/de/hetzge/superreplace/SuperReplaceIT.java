package de.hetzge.superreplace;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

import de.hetzge.superreplace.demo.Base;
import de.hetzge.superreplace.demo.Special;
import de.hetzge.superreplace.demo.Special2;

public class SuperReplaceIT {

	@Test
	public void test() {
		final Replacement replacement = new Replacement(Special.class);
		final ClassFilterPredicate classFilterPredicate = new ClassFilterPredicate.Builder()
				.withIncludeClassNames(Arrays.asList(SuperReplaceIT.class.getName())).build();

		// before transformation (normal behavior)
		assertEquals("xyz", getValue());
		assertEquals("xyz", getInnerValue());
		assertEquals("xyz", new Inner().getValue());
		assertEquals("special2 xyz", getSpecial2Value());
		assertEquals("innerinner", getInnerSpecialValue());
		assertFalse(isSpecial());

		// transformation
		SuperReplace.retransform(Arrays.asList(replacement), classFilterPredicate);

		// after transformation
		assertEquals("class to replace should be replaced with replacement class", "special xyz", getValue());
		assertEquals("usage of the replaced class in the replacement class should not be transformed", "xyz",
				getInnerValue());
		assertEquals("inner classes should also be transformed", "special xyz", new Inner().getValue());
		assertEquals("other classes extending replaced classes should normally behave", "special2 xyz",
				getSpecial2Value());
		assertEquals("special innerinner", getInnerSpecialValue());
		assertTrue(isSpecial());
	}

	/*
	 * ### The following methods bytecode will be transformed by the test ###
	 */

	private String getValue() {
		return new Base("xyz").getValue();
	}

	private String getInnerValue() {
		return new Special("").new SpecialInner().getBaseValue();
	}

	private String getInnerSpecialValue() {
		return new InnerSpecial("inner").getValue();
	}

	private String getSpecial2Value() {
		return new Special2("xyz").getValue();
	}

	private boolean isSpecial() {
		return new Base("xyz") instanceof Special;
	}

	private class Inner {
		public String getValue() {
			return new Base("xyz").getValue();
		}
	}

	public class InnerSpecial extends Base {

		/**
		 * <p>
		 * This is a special case. We are instantiating a instance of a class to replace
		 * inside the super call to a constructor of a class to replace.
		 * </p>
		 * The bytecode looks something like this:
		 * 
		 * <pre>
		 * LINENUMBER 93 L1
		 * ALOAD 0
		 * NEW de/hetzge/superreplace/demo/Base
		 * DUP
		 * ALOAD 2
		 * INVOKESPECIAL de/hetzge/superreplace/demo/Base.<init>(Ljava/lang/String;)V
		 * INVOKEVIRTUAL de/hetzge/superreplace/demo/Base.getValue()Ljava/lang/String;
		 * INVOKESPECIAL de/hetzge/superreplace/demo/Base.<init>(Ljava/lang/String;)V
		 * </pre>
		 * 
		 * The first INVOKESPECIAL have to be replaced, but the second should be
		 * untouched.
		 */
		public InnerSpecial(String value) {
			super(new Base(value).getValue());
		}

		@Override
		public String getValue() {
			return super.getValue() + "inner";
		}
	}
}
