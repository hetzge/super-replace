package de.hetzge.superreplace.demo;

public class Special2 extends Base {

	public Special2(String value) {
		super(value);
	}

	@Override
	public String getValue() {
		return "special2 " + super.getValue();
	}
}
