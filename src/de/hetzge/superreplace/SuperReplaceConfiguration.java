package de.hetzge.superreplace;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.Properties;

/**
 * Represents the arguments passed to super-replace. Provides methods to access
 * and parse values.
 */
public class SuperReplaceConfiguration {
	private final Properties properties;

	/**
	 * Constructor to create {@link SuperReplaceConfiguration} with a {@link String}
	 * containing the arguments in a property like format.
	 * 
	 * @param arguments see {@link #parseArguments(String)}
	 */
	public SuperReplaceConfiguration(String arguments) {
		this(parseArguments(arguments != null ? arguments : ""));
	}

	/**
	 * Constructor to directly pass {@link Properties} object.
	 */
	public SuperReplaceConfiguration(Properties properties) {
		this.properties = properties;
	}

	/**
	 * Represents the value behind the argument 'scanPackages'.
	 * 
	 * @return A list contains package names where super-replace annotations should
	 *         be scanned for. The package names have to be interpreted in a
	 *         recursive way (which means that the package 'com.main' represents
	 *         'com.main' and all its subpackages). Default: empty list.
	 * @see Utils#parseCommaSeperatedValue(String)
	 */
	public List<String> getScanPackageNames() {
		final String scanPackagesValue = properties.getProperty("scanPackages", "");
		return Utils.parseCommaSeperatedValue(scanPackagesValue);
	}

	/**
	 * Represents the value behind the argument 'includePackages'.
	 * 
	 * @return A list contains package names where the classes should be
	 *         transformed. The package names have to be interpreted in a recursive
	 *         way (which means that the package 'com.main' represents 'com.main'
	 *         and all its subpackages). Default: empty list.
	 * @see Utils#parseCommaSeperatedValue(String)
	 */
	public List<String> getIncludePackageNames() {
		final String includePackagesValue = properties.getProperty("includePackages", "");
		return Utils.parseCommaSeperatedValue(includePackagesValue);
	}

	/**
	 * Represents the value behind the argument 'excludePackages'.
	 * 
	 * @return A list contains package names where the classes should NOT be
	 *         transformed. The package names have to be interpreted in a recursive
	 *         way (which means that the package 'com.main' represents 'com.main'
	 *         and all its subpackages). Default: empty list.
	 * @see Utils#parseCommaSeperatedValue(String)
	 */
	public List<String> getExcludePackageNames() {
		final String excludePackagesValue = properties.getProperty("excludePackages", "");
		return Utils.parseCommaSeperatedValue(excludePackagesValue);
	}

	/**
	 * Represents the value behind the argument 'includeClasses'.
	 * 
	 * @return A list contains class names (like from {@link Class#getName()}) which
	 *         should be transformed. Default: empty list.
	 * @see Utils#parseCommaSeperatedValue(String)
	 */
	public List<String> getIncludeClassNames() {
		final String includeClassesValue = properties.getProperty("includeClasses", "");
		return Utils.parseCommaSeperatedValue(includeClassesValue);
	}

	/**
	 * Represents the value behind the argument 'excludeClasses'.
	 * 
	 * @return A list contains class names (like from {@link Class#getName()}) which
	 *         should NOT be transformed. Default: empty list.
	 * @see Utils#parseCommaSeperatedValue(String)
	 */
	public List<String> getExcludeClassNames() {
		final String excludeClassesValue = properties.getProperty("excludeClasses", "");
		return Utils.parseCommaSeperatedValue(excludeClassesValue);
	}

	/**
	 * Creates a {@link ClassFilterPredicate} from the current arguments.
	 */
	public ClassFilterPredicate createClassFilterPredicate() {
		return new ClassFilterPredicate.Builder().withIncludePackageNames(getIncludePackageNames())
				.withExcludePackageNames(getExcludePackageNames()).withIncludeClassNames(getIncludeClassNames())
				.withExcludeClassNames(getExcludeClassNames()).build();
	}

	@Override
	public String toString() {
		return String.format("SuperReplaceConfiguration [properties=%s]", properties);
	}

	/**
	 * Parse <code>agentArguments</code> to a {@link Properties} object.
	 * 
	 * @param agentArguments A {@link String} in a {@link Properties} compatible
	 *                       format. Instead of linebreaks between the values
	 *                       semicolons (';') are possible, too.
	 * @return The parsed arguments.
	 */
	private static Properties parseArguments(String agentArguments) {
		try {
			final Properties properties = new Properties();
			properties.load(new StringReader(agentArguments.replace(';', '\n')));

			return properties;
		} catch (IOException e) {
			throw new IllegalStateException(String.format("Can't read properties from arguments '%s'", agentArguments),
					e);
		}
	}
}