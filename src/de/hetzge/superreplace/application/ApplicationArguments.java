package de.hetzge.superreplace.application;

import java.io.File;
import java.io.Serializable;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import de.hetzge.superreplace.Utils;

public class ApplicationArguments implements Serializable {

	private final Properties properties;

	public ApplicationArguments(String[] arguments) {
		this.properties = parseArguments(arguments);
	}

	/**
	 * Represents the value behind the argument 'files'.
	 * 
	 * @return The files which should be transformed.
	 */
	public List<File> getClassFilesToTransform() {
		final String filesValue = properties.getProperty("files", "");
		return Utils.parseCommaSeperatedValue(filesValue).stream()
				.map(fileValue -> filesValue.replaceAll("\"", "").trim()).map(File::new).collect(Collectors.toList());
	}

	/**
	 * Represents the value behind the argument 'classes'.
	 * 
	 * @return The {@link Class}s which should replace their super classes.
	 */
	public List<Class<?>> getReplacementClasses() {
		final String classesValue = properties.getProperty("classes", "");
		return Utils.parseCommaSeperatedValue(classesValue).stream().map(Utils::parseClass)
				.collect(Collectors.toList());
	}

	@Override
	public String toString() {
		return String.format("ApplicationArguments [properties=%s]", properties);
	}

	private static Properties parseArguments(String[] arguments) {
		if (arguments.length % 2 != 0) {
			throw new IllegalArgumentException("The length of arguments is not valid. Must be divisible by 2");
		}

		final Properties properties = new Properties();

		for (int i = 0; i < arguments.length; i += 2) {
			final String key = arguments[i];
			final String value = arguments[i + 1];
			properties.put(key, value);
		}

		return properties;
	}
}
