package de.hetzge.superreplace;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import de.hetzge.superreplace.demo.Base;
import de.hetzge.superreplace.demo.Special;

public class ReplaceClassUsageIT {

	@Test
	public void test() throws InstantiationException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, NoSuchMethodException, SecurityException {
		final Replacement replacement = new Replacement(Special.class, true);
		final ClassFilterPredicate classFilterPredicate = new ClassFilterPredicate.Builder()
				.withIncludeClassNames(Arrays.asList(ReplaceClassUsageIT.class.getName())).build();

		// transformation
		SuperReplace.retransform(Arrays.asList(replacement), classFilterPredicate);

		Assert.assertEquals("special SPECIAL", instantiateSimple());
		Assert.assertEquals("special BASE", instantiateWithParameter());
	}

	/*
	 * ### The following methods bytecode will be transformed by the test ###
	 */

	private String instantiateSimple() throws InstantiationException, IllegalAccessException {
		return Base.class.newInstance().getValue();
	}

	private String instantiateWithParameter() throws InstantiationException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		return Base.class.getConstructor(String.class).newInstance("BASE").getValue();
	}
}
