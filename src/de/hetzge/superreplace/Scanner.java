package de.hetzge.superreplace;

import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.reflections.Reflections;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

public final class Scanner {

	private Scanner() {
	}

	public static List<Replacement> scanReplaceAnnotations() {
		return scanReplaceAnnotations(Arrays.asList(""));
	}

	public static List<Replacement> scanReplaceAnnotations(Collection<String> scanPackages) {
		final ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
		configurationBuilder.setUrls(getScanUrls(scanPackages));
		configurationBuilder.setScanners(new TypeAnnotationsScanner());
		configurationBuilder.setInputsFilter(
				new FilterBuilder().includePackage(scanPackages.toArray(new String[scanPackages.size()])));

		final Reflections reflections = new Reflections(configurationBuilder);
		final Set<Class<?>> annotatedClasses = reflections.getTypesAnnotatedWith(Replace.class, true);

		return annotatedClasses.stream().map(Replacement::new).collect(Collectors.toList());
	}

	private static Collection<URL> getScanUrls(Collection<String> packages) {
		if (packages != null && !packages.isEmpty()) {
			return packages.stream().map(ClasspathHelper::forPackage).flatMap(Collection::stream)
					.collect(Collectors.toList());
		} else {
			return ClasspathHelper.forClassLoader();
		}
	}
}
