package de.hetzge.superreplace;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;

/**
 * Filter predicate to distinct {@link Class}es. It allows to include and
 * exclude classes by different rules (by package (recursive) and name).
 * 
 * <p>
 * Use the {@link Builder} to create a instance.
 * </p>
 */
public class ClassFilterPredicate implements Predicate<String>, Serializable {

	private final Set<String> includePackageNames;
	private final Set<String> excludePackageNames;

	private final Set<String> includeClassNames;
	private final Set<String> excludeClassNames;

	private ClassFilterPredicate(Builder builder) {
		this.includePackageNames = toSetOrEmpty(builder.includePackageNames);
		this.excludePackageNames = toSetOrEmpty(builder.excludePackageNames);
		this.includeClassNames = toSetOrEmpty(builder.includeClassNames);
		this.excludeClassNames = toSetOrEmpty(builder.excludeClassNames);
	}

	@Override
	public boolean test(String className) {
		return className != null && (isIncludedPackage(className) && !isExcludedPackage(className))
				&& (isIncludeClass(className) && !isExcludeClass(className));
	}

	private boolean isIncludedPackage(String className) {
		return includePackageNames.isEmpty() || includePackageNames.stream().anyMatch(className::startsWith);
	}

	private boolean isExcludedPackage(String className) {
		return excludePackageNames.stream().anyMatch(className::startsWith);
	}

	private boolean isIncludeClass(String className) {
		return includeClassNames.isEmpty() || includeClassNames.stream().anyMatch(includeClassName -> {
			return className.equals(includeClassName) || Utils.isInnerClassOf(className, includeClassName);
		});
	}

	private boolean isExcludeClass(String className) {
		return excludeClassNames.stream().anyMatch(excludeClassName -> {
			return className.equals(excludeClassName) || Utils.isInnerClassOf(className, excludeClassName);
		});
	}

	@Override
	public String toString() {
		return "ClassFilterPredicate [includePackageNames=" + includePackageNames + ", excludePackageNames="
				+ excludePackageNames + ", includeClassNames=" + includeClassNames + ", excludeClassNames="
				+ excludeClassNames + "]";
	}

	/**
	 * Helper method which converts the given {@link Collection} to a {@link Set} or
	 * return a empty {@link Set} if the {@link Collection} is <code>null</code>.
	 * 
	 * @param collection the {@link Collection} to convert to {@link Set}
	 * @return {@link Set} containing the values from the given {@link Collection}.
	 *         Never <code>null</code>.
	 */
	private static <T> Set<T> toSetOrEmpty(Collection<T> collection) {
		return collection != null ? new HashSet<>(collection) : Collections.emptySet();
	}

	public static class Builder {
		private final Set<String> includePackageNames;
		private final Set<String> excludePackageNames;

		private final Set<String> includeClassNames;
		private final Set<String> excludeClassNames;

		public Builder() {
			this.includePackageNames = new HashSet<>();
			this.excludePackageNames = new HashSet<>();

			this.includeClassNames = new HashSet<>();
			this.excludeClassNames = new HashSet<>();
		}

		public Builder withIncludePackageNames(Collection<String> includePackageNames) {
			this.includePackageNames.addAll(includePackageNames);
			return this;
		}

		public Builder withExcludePackageNames(Collection<String> excludePackageNames) {
			this.excludePackageNames.addAll(excludePackageNames);
			return this;
		}

		public Builder withIncludeClassNames(Collection<String> includeClassNames) {
			this.includeClassNames.addAll(includeClassNames);
			return this;
		}

		public Builder withExcludeClassNames(Collection<String> excludeClassNames) {
			this.excludeClassNames.addAll(excludeClassNames);
			return this;
		}

		public ClassFilterPredicate build() {
			return new ClassFilterPredicate(this);
		}
	}
}
