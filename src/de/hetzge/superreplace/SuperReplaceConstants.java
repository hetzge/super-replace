package de.hetzge.superreplace;

import org.objectweb.asm.Opcodes;

public final class SuperReplaceConstants {

	public static final int API = Opcodes.ASM6;

	private SuperReplaceConstants() {
	}
}
