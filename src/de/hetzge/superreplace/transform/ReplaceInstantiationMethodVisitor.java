package de.hetzge.superreplace.transform;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Type;
import org.pmw.tinylog.Logger;

import de.hetzge.superreplace.Replacement;
import javassist.bytecode.Opcode;

/**
 * Visitor responsible for visiting the instructions inside a method.
 */
public class ReplaceInstantiationMethodVisitor extends MethodVisitor {

	/**
	 * The replacement the visitor is responsible for.
	 */
	private final Replacement replacement;

	/**
	 * The name of the method the visitor is visiting.
	 */
	private final String methodName;

	private boolean firstSuperConstructorCall = false;
	private int newCounter = 0;

	public ReplaceInstantiationMethodVisitor(int api, MethodVisitor mv, Replacement replacement, String methodName) {
		super(api, mv);
		this.replacement = replacement;
		this.methodName = methodName;
	}

	@Override
	public void visitTypeInsn(int opcode, String type) {
		if (opcode == Opcode.NEW && type.equals(Type.getInternalName(replacement.getClassToReplace()))) {
			newCounter++;
			final Class<?> replacementClass = replacement.getReplacementClass();
			super.visitTypeInsn(opcode, Type.getInternalName(replacementClass));
			Logger.debug(String.format("visitTypeInsn in method '%s' for '%s'", methodName, replacementClass));
		} else {
			super.visitTypeInsn(opcode, type);
		}
	}

	@Override
	public void visitMethodInsn(int opcode, String owner, String name, String desc, boolean itf) {
		if (!isSuperConstructorMethodInsn(owner, name) && name.equals("<init>")
				&& owner.equals(Type.getInternalName(replacement.getClassToReplace()))) {
			newCounter--;
			final Class<?> replacementClass = replacement.getReplacementClass();
			super.visitMethodInsn(opcode, Type.getInternalName(replacementClass), name, desc, itf);
			Logger.debug(String.format("visitMethodInsn in method '%s' for '%s'", methodName, replacementClass));
		} else {
			super.visitMethodInsn(opcode, owner, name, desc, itf);
		}
	}

	@Override
	public void visitLdcInsn(Object cst) {
		if (replacement.isReplaceClassUsage() && cst instanceof Type
				&& ((Type) cst).getInternalName().equals(Type.getInternalName(replacement.getClassToReplace()))) {
			final Class<?> replacementClass = replacement.getReplacementClass();
			super.visitLdcInsn(Type.getType(replacementClass));
			Logger.debug(String.format("visitMethodInsn in method '%s' for '%s'", methodName, replacementClass));
		} else {
			super.visitLdcInsn(cst);
		}
	}

	private boolean isSuperConstructorMethodInsn(String owner, String name) {
		final boolean result = !firstSuperConstructorCall && methodName.equals("<init>") && name.equals("<init>")
				&& owner.equals(Type.getInternalName(replacement.getClassToReplace())) && newCounter == 0;

		if (result) {
			firstSuperConstructorCall = true;
		}

		return result;
	}
}