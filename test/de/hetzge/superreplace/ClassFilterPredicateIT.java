package de.hetzge.superreplace;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class ClassFilterPredicateIT {

	@Test
	public void test() {
		final List<String> includePackageNames = Arrays.asList("my.package");
		final List<String> excludePackageNames = Arrays.asList("my.package.ignore");
		final List<String> excludeClassNames = Arrays.asList("my.package.Something");

		final ClassFilterPredicate classFilterPredicate = new ClassFilterPredicate.Builder()
				.withIncludePackageNames(includePackageNames).withExcludePackageNames(excludePackageNames)
				.withExcludeClassNames(excludeClassNames).build();

		Assert.assertFalse(classFilterPredicate.test(null));
		Assert.assertTrue(classFilterPredicate.test("my.package.Main"));
		Assert.assertTrue(classFilterPredicate.test("my.package.service.Service"));
		Assert.assertFalse(classFilterPredicate.test("my.package.Something"));
		Assert.assertFalse(classFilterPredicate.test("my.package.ignore.Example"));
	}

	@Test
	public void testWithIncludeClassNames() {
		final List<String> includeClassNames = Arrays.asList("my.package.ignore.Important");

		final ClassFilterPredicate classFilterPredicateWithIncludeClassName = new ClassFilterPredicate.Builder()
				.withIncludeClassNames(includeClassNames).build();

		Assert.assertTrue(classFilterPredicateWithIncludeClassName.test("my.package.ignore.Important"));
		Assert.assertFalse(classFilterPredicateWithIncludeClassName.test("my.package.ignore.NotImportant"));
		Assert.assertFalse(classFilterPredicateWithIncludeClassName.test("my.Main"));
	}
}
