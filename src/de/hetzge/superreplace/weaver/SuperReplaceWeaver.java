package de.hetzge.superreplace.weaver;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.instrument.IllegalClassFormatException;
import java.util.List;

import javax.activation.DataSource;

import org.apache.commons.io.IOUtils;
import org.apache.commons.weaver.model.AnnotatedElements;
import org.apache.commons.weaver.model.ScanRequest;
import org.apache.commons.weaver.model.Scanner;
import org.apache.commons.weaver.model.WeavableClass;
import org.apache.commons.weaver.model.WeaveEnvironment;
import org.apache.commons.weaver.spi.Weaver;
import org.apache.xbean.asm5.Type;
import org.pmw.tinylog.Logger;

import de.hetzge.superreplace.ClassFilterPredicate;
import de.hetzge.superreplace.Replacement;
import de.hetzge.superreplace.SuperReplaceConfiguration;
import de.hetzge.superreplace.Utils;
import de.hetzge.superreplace.transform.ReplaceInstantiationClassFileTransformer;

public class SuperReplaceWeaver implements Weaver {

	@Override
	public boolean process(WeaveEnvironment environment, Scanner scanner) {

		Logger.info("begin super-replace weaving");

		try {
			final List<Replacement> replacements = getReplacements();
			if (replacements.isEmpty()) {
				Logger.info("No replacements found. Skip super-replace weaving ...");
				return false;
			}
			Utils.logReplacementConfigs(replacements);
			Utils.logMissingConstructors(replacements);
			Utils.logConflictingReplacements(replacements);

			final SuperReplaceConfiguration configuration = new SuperReplaceConfiguration(environment.config);
			Logger.info("configuration: " + configuration);
			final ClassFilterPredicate classFilterPredicate = configuration.createClassFilterPredicate();

			final ReplaceInstantiationClassFileTransformer transformer = new ReplaceInstantiationClassFileTransformer(
					replacements, classFilterPredicate);

			final AnnotatedElements<WeavableClass<?>> weavableClasses = scanner.scan(new ScanRequest()).getClasses();
			for (WeavableClass<?> weavableClass : weavableClasses) {
				final Class<?> clazz = weavableClass.getTarget();
				final String internalName = Type.getInternalName(clazz);
				final DataSource dataSource = environment.getClassfile(clazz);

				try (final InputStream inputStream = dataSource.getInputStream()) {

					final byte[] transformed = transformer.transform(null, internalName, clazz, null,
							IOUtils.toByteArray(inputStream));
					if (transformed != null) {
						try (final OutputStream outputStream = dataSource.getOutputStream()) {
							IOUtils.write(transformed, outputStream);
						}
					}
				}
			}

		} catch (IOException | IllegalClassFormatException e) {
			throw new IllegalStateException("Error while super-replace weaving", e);
		}

		Logger.info("finished super-replace weaving");

		return true;
	}

	private List<Replacement> getReplacements() {
		/*
		 * I don't use the weavers scanner here. It seems it only scans the weavers
		 * target folder (Is this correct ?). We need to scan the complete class path
		 * because annotations could be outside the target folder.
		 */
		return de.hetzge.superreplace.Scanner.scanReplaceAnnotations();
	}
}
