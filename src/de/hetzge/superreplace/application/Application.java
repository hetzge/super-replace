package de.hetzge.superreplace.application;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.instrument.IllegalClassFormatException;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;

import de.hetzge.superreplace.Replacement;
import de.hetzge.superreplace.transform.ReplaceInstantiationClassFileTransformer;

public class Application {

	public static void main(String[] arguments) throws IOException, IllegalClassFormatException {

		final ApplicationArguments applicationArguments = new ApplicationArguments(arguments);

		final List<File> classFilesToTransform = applicationArguments.getClassFilesToTransform();
		final List<Class<?>> replacementClasses = applicationArguments.getReplacementClasses();
		final List<Replacement> replacements = replacementClasses.stream().map(Replacement::new)
				.collect(Collectors.toList());

		for (File classFileToTransform : classFilesToTransform) {

			// get the bytes from the .class file
			final byte[] classBytes = Files.readAllBytes(classFileToTransform.toPath());

			// create the transformer
			final ReplaceInstantiationClassFileTransformer replaceInstantiationClassFileTransformer = new ReplaceInstantiationClassFileTransformer(
					replacements, c -> true);

			// do the transformation
			final byte[] transformedClassBytes = replaceInstantiationClassFileTransformer.transform(null, "my/class",
					null, null, classBytes);

			// write the bytes back to a file
			try (FileOutputStream fos = new FileOutputStream(classFileToTransform.getName() + ".transformed")) {
				fos.write(transformedClassBytes);
			}
		}
	}
}
