
package de.hetzge.superreplace.instrumentation;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.Instrumentation;
import java.lang.instrument.UnmodifiableClassException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.pmw.tinylog.Logger;

import de.hetzge.superreplace.Replacement;
import de.hetzge.superreplace.Utils;
import de.hetzge.superreplace.transform.ReplaceInstantiationClassFileTransformer;

/**
 * This is the entry point (contains the 'premain' method) of the super-replace
 * agent.
 */
public class Agent {

	/**
	 * The {@link Instrumentation} object which is necessary for class
	 * transformation. This field holds it for later use.
	 */
	private static Instrumentation instrumentation;

	/**
	 * The currently active transformer. Should be <code>null</code> if no
	 * transformer is active. Active means added to the instrumentation.
	 */
	private static ClassFileTransformer activeTransformer;

	/**
	 * Main method for self attaching agent. Mainly used for testing.
	 * 
	 * @see #premain(String, Instrumentation)
	 */
	public static void agentmain(String agentArguments, Instrumentation instrumentation) {
		premain(agentArguments, instrumentation);
	}

	/**
	 * 'premain' method which is called by the vm to provide agent arguments and
	 * {@link Instrumentation} object.
	 * 
	 * @param agentArguments  The arguments passed to the agent. Example:
	 *                        <code>-javaagent:"c:/app/super-replace.jar"=argumentName:argumentValue;otherArgumentName:otherArgumentValue</code>
	 *                        The part after the '=' defines the arguments.
	 * @param instrumentation The {@link Instrumentation} provided by the vm. It is
	 *                        necessary to tranform classes.
	 */
	public static void premain(String agentArguments, Instrumentation instrumentation) {
		if (!instrumentation.isRetransformClassesSupported()) {
			throw new IllegalStateException("JVM is not supporting class retransformation");
		}

		Utils.fixJBossModules();
		
		Agent.instrumentation = instrumentation;
	}

	/**
	 * Applies the super-replace transformations to the currently loaded classes.
	 * 
	 * @param replacements         The configurations which define which classes
	 *                             should be replaced.
	 * @param classFilterPredicate Filter which restricts the classes where the
	 *                             transformation should be applied to. This allows
	 *                             to specify concrete places (packages, classes)
	 *                             where the transformation should be applied to and
	 *                             where not.
	 */
	public synchronized static void retransform(Collection<Replacement> replacements,
			Predicate<String> classFilterPredicate) {
		final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		final Class<?>[] allClasses = instrumentation.getInitiatedClasses(classLoader);
		
		retransform(replacements, classFilterPredicate, Arrays.asList(allClasses));
	}
	
	public synchronized static void retransform(Collection<Replacement> replacements,
			Predicate<String> classFilterPredicate, Collection<Class<?>> classes) {
		try {
			Utils.logReplacementConfigs(replacements);
			Utils.logMissingConstructors(replacements);
			Utils.logConflictingReplacements(replacements);

			final Instrumentation instrumentation = getInstrumentation();

			final List<Class<?>> classesToTransform = classes.stream().filter(instrumentation::isModifiableClass)
					.filter(clazz -> classFilterPredicate.test(clazz.getName())).collect(Collectors.toList());

			Logger.info("{} of {} classes qualified for transformation", classesToTransform.size(), classes.size());

			setActiveTransformer(new ReplaceInstantiationClassFileTransformer(replacements, classFilterPredicate));

			Logger.info("transformer registered");

			if (!classesToTransform.isEmpty()) {
				instrumentation.retransformClasses(classesToTransform.toArray(new Class[classesToTransform.size()]));
			}
		} catch (UnmodifiableClassException e) {
			throw new IllegalStateException("Exception while apply super-replace retransformation.", e);
		}
	}

	/**
	 * Sets a transformer as active transformer. The previously active transformer
	 * will be replaced with the new one. This method ensures that only one
	 * super-replace transformer is active at the same time.
	 */
	private static void setActiveTransformer(ClassFileTransformer transformer) {
		final Instrumentation instrumentation = getInstrumentation();

		if (activeTransformer != null) {
			instrumentation.removeTransformer(activeTransformer);
		}

		Agent.activeTransformer = transformer;
		getInstrumentation().addTransformer(transformer, true);
	}

	/**
	 * Getter for {@link #instrumentation}. Throws a exception if
	 * {@link #instrumentation} is not available.
	 */
	private static Instrumentation getInstrumentation() {
		if (!isInitialized()) {
			throw new IllegalStateException(
					"Try to get instrumentation, but it is null. Eventually the agent is not configured.");
		}

		return instrumentation;
	}

	/**
	 * Checks if the agent is initialized. Otherwise you should check if the agent
	 * is passed as paramter to your application.
	 * 
	 * @return <code>true</code> if the agent is initialzied.
	 */
	public static boolean isInitialized() {
		return instrumentation != null;
	}
}
